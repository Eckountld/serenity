package testing_impl;


import au.com.bytecode.opencsv.CSVReader;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileReader;

public class implements_class {
                   ChromeDriver driver;
    @When("Chrome driver opens up")
    public void whenChromeDriverOpensUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Then("the form will be filled automatically")
    public void thenTheFormWillBeFilledAutomatically() throws Throwable{
        CSVReader read = new CSVReader(new FileReader("C:\\repositories\\cucumber_selenium\\SerenityExample\\wrong_data.csv"));
        String[] cell;
        while((cell = read.readNext())!=null)
        {
            driver.get("http:/rsvp.eohdt.co.za/rsvp.html");
            for (int i = 0; i < 1; i++)
            {
                String FullName = cell[i];
                String Email = cell[i+1];

                driver.findElement(By.name("fullname")).sendKeys(FullName);
                driver.findElement(By.name("email")).sendKeys(Email);
                driver.findElement(By.className("btn")).submit();
            }

        }
    }

    @Then("the chrome driver will close")
    public void thenTheChromeDriverWillClose() {
        driver.close();
    }
}
